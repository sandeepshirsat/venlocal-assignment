<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Meta Data -->
    <meta name="title" content="Venlocal - Link Generator">
    <meta name="description" content="Earn, chat & meet local friends who share your interests, values or skills at any venue in the world!">
    <meta name="author" content="Venlocal | https://venlocal.com/">
    <meta name="keywords" content="chat room, chat, chatroom, dating, Link Generator, earn, games, group chat, hire, icebreaker, jobs, live chat, local, messaging, money, music, payment, private chat, radio player, radio, rent, social network, trade, venues">
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="English">
    <meta name="revisit-after" content="7 days">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="https://venlocal.com/media/favicon/favicon.ico">
    <link rel="apple-touch-icon" sizes="72x72" href="https://venlocal.com/media/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://venlocal.com/media/favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="144x144" href="https://venlocal.com/media/favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="256x256" href="https://venlocal.com/media/favicon/apple-touch-icon-256x256.png" />
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="{{ asset('/css/main.css') }}" rel="stylesheet">
    <script>
        window.base_url = '';
        // let home_url = '{{ url("/") }}';
        let home_url = 'https://venlocal.com';
    </script>
    <!-- Styles -->
    <!-- <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */
            html {
                line-height: 1.15;
                -webkit-text-size-adjust: 100%
            }

            body {
                margin: 0
            }

            a {
                background-color: transparent
            }

            [hidden] {
                display: none
            }

            html {
                font-family: system-ui, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica Neue, Arial, Noto Sans, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol, Noto Color Emoji;
                line-height: 1.5
            }

            *,
            :after,
            :before {
                box-sizing: border-box;
                border: 0 solid #e2e8f0
            }

            a {
                color: inherit;
                text-decoration: inherit
            }

            svg,
            video {
                display: block;
                vertical-align: middle
            }

            video {
                max-width: 100%;
                height: auto
            }

            .bg-white {
                --bg-opacity: 1;
                background-color: #fff;
                background-color: rgba(255, 255, 255, var(--bg-opacity))
            }

            .bg-gray-100 {
                --bg-opacity: 1;
                background-color: #f7fafc;
                background-color: rgba(247, 250, 252, var(--bg-opacity))
            }

            .border-gray-200 {
                --border-opacity: 1;
                border-color: #edf2f7;
                border-color: rgba(237, 242, 247, var(--border-opacity))
            }

            .border-t {
                border-top-width: 1px
            }

            .flex {
                display: flex
            }

            .grid {
                display: grid
            }

            .hidden {
                display: none
            }

            .items-center {
                align-items: center
            }

            .justify-center {
                justify-content: center
            }

            .font-semibold {
                font-weight: 600
            }

            .h-5 {
                height: 1.25rem
            }

            .h-8 {
                height: 2rem
            }

            .h-16 {
                height: 4rem
            }

            .text-sm {
                font-size: .875rem
            }

            .text-lg {
                font-size: 1.125rem
            }

            .leading-7 {
                line-height: 1.75rem
            }

            .mx-auto {
                margin-left: auto;
                margin-right: auto
            }

            .ml-1 {
                margin-left: .25rem
            }

            .mt-2 {
                margin-top: .5rem
            }

            .mr-2 {
                margin-right: .5rem
            }

            .ml-2 {
                margin-left: .5rem
            }

            .mt-4 {
                margin-top: 1rem
            }

            .ml-4 {
                margin-left: 1rem
            }

            .mt-8 {
                margin-top: 2rem
            }

            .ml-12 {
                margin-left: 3rem
            }

            .-mt-px {
                margin-top: -1px
            }

            .max-w-6xl {
                max-width: 72rem
            }

            .min-h-screen {
                min-height: 100vh
            }

            .overflow-hidden {
                overflow: hidden
            }

            .p-6 {
                padding: 1.5rem
            }

            .py-4 {
                padding-top: 1rem;
                padding-bottom: 1rem
            }

            .px-6 {
                padding-left: 1.5rem;
                padding-right: 1.5rem
            }

            .pt-8 {
                padding-top: 2rem
            }

            .fixed {
                position: fixed
            }

            .relative {
                position: relative
            }

            .top-0 {
                top: 0
            }

            .right-0 {
                right: 0
            }

            .shadow {
                box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .1), 0 1px 2px 0 rgba(0, 0, 0, .06)
            }

            .text-center {
                text-align: center
            }

            .text-gray-200 {
                --text-opacity: 1;
                color: #edf2f7;
                color: rgba(237, 242, 247, var(--text-opacity))
            }

            .text-gray-300 {
                --text-opacity: 1;
                color: #e2e8f0;
                color: rgba(226, 232, 240, var(--text-opacity))
            }

            .text-gray-400 {
                --text-opacity: 1;
                color: #cbd5e0;
                color: rgba(203, 213, 224, var(--text-opacity))
            }

            .text-gray-500 {
                --text-opacity: 1;
                color: #a0aec0;
                color: rgba(160, 174, 192, var(--text-opacity))
            }

            .text-gray-600 {
                --text-opacity: 1;
                color: #718096;
                color: rgba(113, 128, 150, var(--text-opacity))
            }

            .text-gray-700 {
                --text-opacity: 1;
                color: #4a5568;
                color: rgba(74, 85, 104, var(--text-opacity))
            }

            .text-gray-900 {
                --text-opacity: 1;
                color: #1a202c;
                color: rgba(26, 32, 44, var(--text-opacity))
            }

            .underline {
                text-decoration: underline
            }

            .antialiased {
                -webkit-font-smoothing: antialiased;
                -moz-osx-font-smoothing: grayscale
            }

            .w-5 {
                width: 1.25rem
            }

            .w-8 {
                width: 2rem
            }

            .w-auto {
                width: auto
            }

            .grid-cols-1 {
                grid-template-columns: repeat(1, minmax(0, 1fr))
            }

            @media (min-width:640px) {
                .sm\:rounded-lg {
                    border-radius: .5rem
                }

                .sm\:block {
                    display: block
                }

                .sm\:items-center {
                    align-items: center
                }

                .sm\:justify-start {
                    justify-content: flex-start
                }

                .sm\:justify-between {
                    justify-content: space-between
                }

                .sm\:h-20 {
                    height: 5rem
                }

                .sm\:ml-0 {
                    margin-left: 0
                }

                .sm\:px-6 {
                    padding-left: 1.5rem;
                    padding-right: 1.5rem
                }

                .sm\:pt-0 {
                    padding-top: 0
                }

                .sm\:text-left {
                    text-align: left
                }

                .sm\:text-right {
                    text-align: right
                }
            }

            @media (min-width:768px) {
                .md\:border-t-0 {
                    border-top-width: 0
                }

                .md\:border-l {
                    border-left-width: 1px
                }

                .md\:grid-cols-2 {
                    grid-template-columns: repeat(2, minmax(0, 1fr))
                }
            }

            @media (min-width:1024px) {
                .lg\:px-8 {
                    padding-left: 2rem;
                    padding-right: 2rem
                }
            }

            @media (prefers-color-scheme:dark) {
                .dark\:bg-gray-800 {
                    --bg-opacity: 1;
                    background-color: #2d3748;
                    background-color: rgba(45, 55, 72, var(--bg-opacity))
                }

                .dark\:bg-gray-900 {
                    --bg-opacity: 1;
                    background-color: #1a202c;
                    background-color: rgba(26, 32, 44, var(--bg-opacity))
                }

                .dark\:border-gray-700 {
                    --border-opacity: 1;
                    border-color: #4a5568;
                    border-color: rgba(74, 85, 104, var(--border-opacity))
                }

                .dark\:text-white {
                    --text-opacity: 1;
                    color: #fff;
                    color: rgba(255, 255, 255, var(--text-opacity))
                }

                .dark\:text-gray-400 {
                    --text-opacity: 1;
                    color: #cbd5e0;
                    color: rgba(203, 213, 224, var(--text-opacity))
                }
            }
        </style>

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style> -->
    <style>
        body {
            background: #141e27;
            color: #fff;
        }

        .mt-3,
        .my-3 {
            margin-top: 0rem !important;
        }

        .btn-success,
        .btn-success.hover,
        .btn-success:focus,
        .btn-success:active,
        .btn-success:hover {
            border-color: #9C27B0;
            border-style: none;
            background-color: #9C27B0 !important;
            background-image: linear-gradient(-45deg, #9C27B0 0%, #03A9F4 100%) !important;
            color: #E0E0E0;
        }
    </style>
</head>

<body cz-shortcut-listen="true">

    <div class="jumbotron container my-3 py-3" style="background-color: #1F2D41">
        <h5 class="text-center">Generate a Venlocal message link to contact a user, to activate your bot or join your
            list. Embed as button link on your website, use QR code for cards, shorten to send link via social or in
            email signature.</h5>
        <hr />
        <div class="row">
            <div class="col-md-4 col-12">
                <h4>1. Chatroom</h4>
                <div class="form-group">
                    <select class="form-control input-lg" id="_walink_code">
                        <option value="office-studio" selected="">Office/Studio</option>
                        <option data-countrycode="US" value="1">USA (+1)</option>
                        <option data-countrycode="ID" value="62">Indonesia (+62)</option>
                        <option data-countrycode="MY" value="60">Malaysia (+60)</option>
                        <optgroup label="Other countries">
                            <option data-countrycode="DZ" value="213">Algeria (+213)</option>
                            <option data-countrycode="AD" value="376">Andorra (+376)</option>
                            <option data-countrycode="AO" value="244">Angola (+244)</option>
                            <option data-countrycode="AI" value="1264">Anguilla (+1264)</option>
                            <option data-countrycode="AG" value="1268">Antigua &amp; Barbuda (+1268)</option>
                            <option data-countrycode="AR" value="54">Argentina (+54)</option>
                            <option data-countrycode="AM" value="374">Armenia (+374)</option>
                            <option data-countrycode="AW" value="297">Aruba (+297)</option>
                            <option data-countrycode="AU" value="61">Australia (+61)</option>
                            <option data-countrycode="AT" value="43">Austria (+43)</option>
                            <option data-countrycode="AZ" value="994">Azerbaijan (+994)</option>
                            <option data-countrycode="BS" value="1242">Bahamas (+1242)</option>
                            <option data-countrycode="BH" value="973">Bahrain (+973)</option>
                            <option data-countrycode="BD" value="880">Bangladesh (+880)</option>
                            <option data-countrycode="BB" value="1246">Barbados (+1246)</option>
                            <option data-countrycode="BY" value="375">Belarus (+375)</option>
                            <option data-countrycode="BE" value="32">Belgium (+32)</option>
                            <option data-countrycode="BZ" value="501">Belize (+501)</option>
                            <option data-countrycode="BJ" value="229">Benin (+229)</option>
                            <option data-countrycode="BM" value="1441">Bermuda (+1441)</option>
                            <option data-countrycode="BT" value="975">Bhutan (+975)</option>
                        </optgroup>
                    </select>
                </div>
                <h6>You must join the selected chatroom for it to work</h6>
            </div>
            <div class="col-md-4 col-12">
                <h4>2. Username/Phone</h4>
                <div class="form-group">
                    <input type="text" class="form-control input-lg" name="phone" id="_walink_phone" placeholder="XXXXXXXXXXXXX" maxlength="15" />
                </div>
            </div>
            <div class="col-md-4 col-12">
                <h4>3. List ID (Optional)</h4>
                <div class="form-group">
                    <input type="text" class="form-control input-lg" name="list" id="_list_id" placeholder="XXXXX" maxlength="10" />
                </div>
                <h6>You must create a list first for it to work</h6>
            </div>
        </div>
    </div>
    <div class="jumbotron container my-3 py-3" style="background-color: #1F2D41">
        <h4 class="text-center">Message</h4>
        <div class="input-group">
            <div class="toolbar">
                <div class="item" data-tool="bold"><b>B</b></div>
                <div class="item" data-tool="italic"><i>I</i></div>
                <div class="item" data-tool="striketrhough"><del>S</del></div>
            </div>
            <textarea style="font-size:20px; " class="form-control input-lg" id="_walink_message" rows="5" placeholder="Enter pre-filled message that will automatically appear in the text field of the chat."></textarea>
        </div>
    </div>
    <div class="container my-3 py-3">
        <button class="btn btn-lg btn-success btn-block" id="_walink_generate">Generate Venlocal Link</button>
    </div>
    <div class="jumbotron container my-3 py-3" style="background-color: #1F2D41; display: none;" id="_walink_result">
        <div class="row">
            <div class="col-md-4">
                <h4>QR Code</h4>
                <img width="100%" id="_walink_qr" />
                <h6>Right click and save image</h6>
            </div>
            <div class="col-md-8 mt-3">
                <h4>Link</h4>
                <div class="input-group">
                    <input type="text" class="form-control input-lg" id="_walink_reslink" />
                </div>
                <div class="input-group pt-2">
					<button class="btn btn-lg btn-success" onclick="copyToClipboard(&#39;#_walink_reslink&#39;)">Copy
						Link</button>
					<a href="https://venlocal.com/send?phone=6283840460580&amp;text=https%3A%2F%2Fapi.whatsapp.com%2Fsend%3Fphone%3D62%26text%3D"
						class="btn btn-lg btn-success mx-2" target="_blank" id="_walink_testlink">Test Link</a>
				</div>
				<br>
				<h4>Button</h4>
				<!-- <div class="input-group">
					<textarea style="font-size:20px; " class="form-control input-lg" id="_walink_reslink" rows="2"
						placeholder="Copy code and embed"></textarea>
				</div> -->
                <div class="input-group">
					<textarea style="font-size:20px; " class="form-control input-lg" id="_walink_embed_code" rows="2"
						placeholder="Copy code and embed" value=""></textarea>
				</div>
				<div class="input-group pt-2">
					<a href="#"
						target="_blank" id="_walink_testlink_button"><img src="{{ asset('storage/button.png')}}">
                    </a>
				</div>
            </div>
        </div>
    </div>
    <!-- Bootstrap core JavaScript  style="background: #01E675"-->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="{{ asset('/js/main.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("img#closed").click(function() {
                $("#bl_banner").hide(90);
            });
        });
    </script>

</body>

</html>