function goToByScroll(id) {
    document.getElementById(id).scrollIntoView({ behavior: "smooth" });
}

function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).val()).select();
    document.execCommand("copy");
    $temp.remove();
    alert("Copied");
}

function generate() {
    let username = $("#_walink_phone").val();
    let chatroom = $("#_walink_code").val();
    let list_id = $("#_list_id").val();
    let message = $("#_walink_message").val();
    if (username.charAt(0) === "0") {
        username = username.slice(1);
    }
    // prepare and set a link for chat 
    var u =
        home_url +
        "/" +
        chatroom +
        "/" +
        username +
        "?" +
        "list=" + encodeURIComponent(list_id) +
        "&text=" +
        encodeURIComponent(message);
    $("#_walink_reslink").val(u);
    // $("#_walink_embed_code").val(u);
    $("#_walink_testlink").attr("href",u);
    $("#_walink_testlink_button").attr("href",u);
    
    // create qr code of link
    $("#_walink_qr").attr(
        "src",
        base_url +
            "https://chart.googleapis.com/chart?chs=500x500&cht=qr&chl=" +
            encodeURIComponent(u) +
            "&choe=UTF-8&chld=H"
    );
    $("#_walink_qrlink").attr(
        "href",
        base_url + "qr.php?url=" + encodeURIComponent(u) + "&dl=1"
    );
    // display qr and generated link section
    $("#_walink_result").show();
    goToByScroll("_walink_result");
}

$(function () {
    $("#_walink_result").hide();
    $("#_walink_generate").click(generate);
});